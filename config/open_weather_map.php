<?php

declare(strict_types=1);

return [
    'api_key' => env('OPEN_WEATHER_MAP_API_KEY'),
    'url_api' => env('OPEN_WEATHER_MAP_URL'),
];
