<?php

declare(strict_types=1);

use App\Http\Controllers\Api\WeatherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function (): void {
    Route::group([
        'prefix' => '/weather',
    ], static function () {
        Route::get('', [WeatherController::class, 'index']);
        Route::post('', [WeatherController::class, 'store']);
        Route::get('/{id}', [WeatherController::class, 'show']);
        Route::put('/{id}', [WeatherController::class, 'update']);
        Route::delete('/{id}', [WeatherController::class, 'destroy']);
        Route::get('/date/{date}', [WeatherController::class, 'getByDate']);
    });
});
