<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WeatherFactory extends Factory
{
    public function definition(): array
    {
        return [
            'date' => $this
                ->faker
                ->dateTimeBetween('-7 days', '+7 days')
                ->format('Y-m-d'),
            'main_description' => $this->faker->text(20),
            'description' => $this->faker->text(100),
            'temp' => $this->faker->randomDigit,
            'feels_like' => $this->faker->randomDigit,
            'temp_min' => $this->faker->randomDigit,
            'temp_max' => $this->faker->randomDigit,
            'pressure' => $this->faker->randomDigit,
            'humidity' => $this->faker->randomDigit,
            'wind_speed' => $this->faker->randomDigit,
            'wind_deg' => $this->faker->randomDigit,
        ];
    }
}
