<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherTable extends Migration
{
    public function up(): void
    {
        Schema::create('weather', function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('city_id');
            $table->date('date');
            $table->string('main_description');
            $table->string('description');
            $table->float('feels_like');
            $table->integer('pressure');
            $table->integer('humidity');
            $table->float('wind_speed');
            $table->float('wind_deg');
            $table->float('temp');
            $table->float('temp_min')->nullable();
            $table->float('temp_max')->nullable();
            $table->timestamps();

            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('weather');
    }
}
