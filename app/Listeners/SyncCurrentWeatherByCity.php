<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Action\Weather\SyncCurrentWeatherByCityAction;
use App\Events\CiteUpdateOrCreate;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncCurrentWeatherByCity implements ShouldQueue
{
    public function __construct(private SyncCurrentWeatherByCityAction $action) {}

    public function handle(CiteUpdateOrCreate $citeUpdateOrCreate)
    {
        $this
            ->action
            ->execute($citeUpdateOrCreate->city);
    }
}

