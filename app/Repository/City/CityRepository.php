<?php

declare(strict_types=1);

namespace App\Repository\City;

use App\Contracts\City\CityRepositoryInterface;
use App\Models\City;
use Illuminate\Database\Eloquent\Collection;

final class CityRepository implements CityRepositoryInterface
{
    public function save(City $city): City
    {
        $city->save();

        return $city;
    }

    public function updateOrCreate(string $cityName, array $fields): City
    {
        return City::updateOrCreate(
            ['name' => $cityName],
            $fields
        );
    }

    public function getAll(): Collection
    {
        return City::all();
    }
}
