<?php

declare(strict_types=1);

namespace App\Repository\Weather;

use App\Contracts\Weather\WeatherRepositoryInterface;
use App\Models\Weather;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

final class WeatherRepository implements WeatherRepositoryInterface
{
    public function create(array $fields): Weather
    {
        return Weather::create($fields);
    }

    public function paginate(
        int $page = self::DEFAULT_PAGE,
        int $perPage = self::DEFAULT_PER_PAGE,
        string $sort = self::DEFAULT_SORT,
        string $direction = self::DEFAULT_DIRECTION
    ): LengthAwarePaginator {
        return Weather::orderBy($sort, $direction)->paginate($perPage, page: $page);
    }

    public function getById(int $id): Weather
    {
        return Weather::findOrFail($id);
    }

    public function save(Weather $weather): Weather
    {
        $weather->save();

        return $weather;
    }

    public function delete(Weather $weather): ?bool
    {
        return $weather->delete();
    }

    public function updateOrCreate(array $compareFields, array $fields): void
    {
        Weather::updateOrCreate($compareFields, $fields);
    }

    public function getByDate(string $date): Collection
    {
        return Weather::where('date', $date)->get();
    }
}
