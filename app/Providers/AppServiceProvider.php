<?php

namespace App\Providers;

use App\Contracts\City\CityRepositoryInterface;
use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Contracts\Weather\WeatherRepositoryInterface;
use App\Repository\City\CityRepository;
use App\Repository\Weather\WeatherRepository;
use App\Services\Gateway\OpenWeatherMap\Configuration;
use App\Services\Gateway\OpenWeatherMap\Exception\ExceptionService;
use App\Services\Gateway\OpenWeatherMap\OpenWeatherMapApi;
use App\Services\Gateway\OpenWeatherMap\OpenWeatherMapClient;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WeatherRepositoryInterface::class, WeatherRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(OpenWeatherMapApiInterface::class, OpenWeatherMapApi::class);
        $this->app->bind(OpenWeatherMapClient::class, function (): OpenWeatherMapClient {
            return new OpenWeatherMapClient(
                new Client(),
                new Configuration(config('open_weather_map.api_key'), config('open_weather_map.url_api')),
                new ExceptionService()
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
