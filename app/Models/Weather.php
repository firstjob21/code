<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class Weather extends Model
{
    use HasFactory;

    protected $with = ['city'];

    protected $fillable = [
        'city_id',
        'date',
        'main_description',
        'description',
        'temp',
        'feels_like',
        'temp_min',
        'temp_max',
        'pressure',
        'humidity',
        'wind_speed',
        'wind_deg',
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }
}
