<?php declare(strict_types=1);

namespace App\Constants\City;

class CityConstants
{
    public const LONDON = 'London';
    public const PARIS = 'Paris';
    public const BERLIN = 'Berlin';
    public const TOKYO = 'Tokyo';
    public const NEW_YORK = 'New York';

    public const CITIES = [
        self::LONDON,
        self::PARIS,
        self::BERLIN,
        self::TOKYO,
        self::NEW_YORK,
    ];
}
