<?php

declare(strict_types=1);


namespace App\Exceptions\Weather;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Throwable;

class WeatherNotFoundException extends ModelNotFoundException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Weather not found.', $code, $previous);
    }
}
