<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Weather;

use Illuminate\Foundation\Http\FormRequest;

final class UpdateWeatherHttpRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'main_description' => 'required|string',
            'description' => 'required|string',
            'temp' => 'required|numeric',
            'feels_like' => 'required|numeric',
            'temp_min' => 'nullable|numeric',
            'temp_max' => 'nullable|numeric',
            'pressure' => 'nullable|numeric',
            'humidity' => 'nullable|numeric',
            'wind_speed' => 'nullable|numeric',
            'wind_deg' => 'nullable|numeric',
        ];
    }
}
