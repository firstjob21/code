<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Weather\DeleteWeatherAction;
use App\Action\Weather\DeleteWeatherDTO;
use App\Action\GetByIdRequest;
use App\Action\Weather\AddWeatherAction;
use App\Action\Weather\AddWeatherDTO;
use App\Action\Weather\GetCollectionDTO;
use App\Action\Weather\GetWeatherByDateAction;
use App\Action\Weather\GetWeatherByDateDTO;
use App\Action\Weather\GetWeatherByIdAction;
use App\Action\Weather\GetWeatherCollectionAction;
use App\Action\Weather\UpdateWeatherAction;
use App\Action\Weather\UpdateWeatherDTO;
use App\Http\Controllers\ApiController;
use App\Http\Presenter\Weather\WeatherArrayPresenter;
use App\Http\Requests\Api\CollectionHttpRequest;
use App\Http\Requests\Api\Weather\AddWeatherHttpRequest;
use App\Http\Requests\Api\Weather\UpdateWeatherHttpRequest;
use App\Http\Response\ApiResponse;

class WeatherController extends ApiController
{
    public function __construct(
        private WeatherArrayPresenter $presenter,
        private AddWeatherAction $addWeatherAction,
        private GetWeatherCollectionAction $getWeatherCollectionAction,
        private GetWeatherByIdAction $getWeatherByIdAction,
        private UpdateWeatherAction $updateWeatherAction,
        private DeleteWeatherAction $deleteWeatherAction,
        private GetWeatherByDateAction $getWeatherByDateAction,
    ) {}

    public function index(CollectionHttpRequest $request): ApiResponse
    {
        $paginator = $this
            ->getWeatherCollectionAction
            ->execute(
                new GetCollectionDTO(
                    (int)$request->query('page'),
                    $request->query('sort'),
                    $request->query('direction')
                )
            )
            ->getPaginator();

        return $this->createPaginatedResponse($paginator, $this->presenter);
    }

    public function store(AddWeatherHttpRequest $request): ApiResponse
    {
        $weather = $this
            ->addWeatherAction
            ->execute(
                new AddWeatherDTO(
                    $request->get('city_id'),
                    $request->get('date'),
                    $request->get('main_description'),
                    $request->get('description'),
                    $request->get('temp'),
                    $request->get('feels_like'),
                    $request->get('temp_min'),
                    $request->get('temp_max'),
                    $request->get('pressure'),
                    $request->get('humidity'),
                    $request->get('wind_speed'),
                    $request->get('wind_deg'),
                )
            )
            ->getWeather();

        return $this->created($this->presenter->present($weather));
    }

    public function show(string $id): ApiResponse
    {
        $weather = $this
            ->getWeatherByIdAction
            ->execute(new GetByIdRequest((int)$id))
            ->getWeather();

        return $this->createSuccessResponse($this->presenter->present($weather));
    }

    public function update(string $id, UpdateWeatherHttpRequest $request): ApiResponse
    {
        $weather = $this
            ->updateWeatherAction
            ->execute(
                new UpdateWeatherDTO(
                    (int) $id,
                    $request->get('date'),
                    $request->get('main_description'),
                    $request->get('description'),
                    $request->get('temp'),
                    $request->get('feels_like'),
                    $request->get('temp_min'),
                    $request->get('temp_max'),
                    $request->get('pressure'),
                    $request->get('humidity'),
                    $request->get('wind_speed'),
                    $request->get('wind_deg'),
                )
            )
            ->getWeather();

        return $this->createSuccessResponse($this->presenter->present($weather));
    }

    public function destroy(string $id): ApiResponse
    {
        $this
            ->deleteWeatherAction
            ->execute(
            new DeleteWeatherDTO(
                (int)$id
            )
        );

        return $this->createDeletedResponse();
    }

    public function getByDate(string $date): ApiResponse
    {
        $weatherCollection = $this
            ->getWeatherByDateAction
            ->execute(new GetWeatherByDateDTO($date))
            ->getCollection();

        return $this->createSuccessResponse(
            $this
            ->presenter
            ->presentCollection($weatherCollection)
        );
    }
}
