<?php

declare(strict_types = 1);

namespace App\Http\Presenter\Weather;

use App\Http\Presenter\City\CityArrayPresenter;
use App\Http\Presenter\CollectionAsArrayPresenter;
use Illuminate\Database\Eloquent\Model;

final class WeatherArrayPresenter extends CollectionAsArrayPresenter
{
    public function __construct(private CityArrayPresenter $cityPresenter) {}

    public function present(Model $model): array
    {
        return [
            'id' => $model->getAttribute('id'),
            'city' => $this->cityPresenter->present($model->city),
            'date' => $model->getAttribute('date'),
            'main_description' => $model->getAttribute('main_description'),
            'description' => $model->getAttribute('description'),
            'temp' => $model->getAttribute('temp'),
            'feels_like' => $model->getAttribute('feels_like'),
            'temp_min' => $model->getAttribute('temp_min'),
            'temp_max' => $model->getAttribute('temp_max'),
            'pressure' => $model->getAttribute('pressure'),
            'humidity' => $model->getAttribute('humidity'),
            'wind_speed' => $model->getAttribute('wind_speed'),
            'wind_deg' => $model->getAttribute('wind_deg'),
        ];
    }
}
