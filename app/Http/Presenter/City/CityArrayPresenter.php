<?php

declare(strict_types=1);

namespace App\Http\Presenter\City;

use Illuminate\Database\Eloquent\Model;

class CityArrayPresenter
{
    public function present(Model $model): array
    {
        return [
            'id' => $model->getAttribute('id'),
            'name' => $model->getAttribute('name'),
            'latitude' => $model->getAttribute('latitude'),
            'longitude' => $model->getAttribute('longitude'),
        ];
    }
}
