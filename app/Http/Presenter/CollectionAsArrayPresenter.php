<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class CollectionAsArrayPresenter implements CollectionAsArrayPresenterInterface
{
    abstract public function present(Model $model): array;

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Model $model) {
                    return $this->present($model);
                }
            )
            ->all();
    }
}
