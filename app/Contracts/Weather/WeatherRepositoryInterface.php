<?php

declare(strict_types=1);

namespace App\Contracts\Weather;

use App\Models\Weather;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface WeatherRepositoryInterface
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_PER_PAGE = 15;
    public const DEFAULT_SORT = 'created_at';
    public const DEFAULT_DIRECTION = 'desc';

    public function create(array $fields): Weather;

    public function getById(int $id): Weather;

    public function save(Weather $weather): Weather;

    public function delete(Weather $weather): ?bool;

    public function updateOrCreate(array $compareFields, array $fields): void;

    public function paginate(
        int $page = self::DEFAULT_PAGE,
        int $perPage = self::DEFAULT_PER_PAGE,
        string $sort = self::DEFAULT_SORT,
        string $direction = self::DEFAULT_DIRECTION
    ): LengthAwarePaginator;
}
