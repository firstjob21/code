<?php

declare(strict_types=1);

namespace App\Contracts\Gateway;

use App\Services\Gateway\OpenWeatherMap\Models\Model;

interface OpenWeatherMapApiInterface
{
    public function getWeatherData(Model $model): array;
}
