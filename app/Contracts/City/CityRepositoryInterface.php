<?php

declare(strict_types=1);

namespace App\Contracts\City;

use App\Models\City;
use Illuminate\Database\Eloquent\Collection;

interface CityRepositoryInterface
{
    public function save(City $city): City;

    public function updateOrCreate(string $cityName, array $fields): City;

    public function getAll(): Collection;
}
