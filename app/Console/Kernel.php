<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\Weather\SyncCurrentWeatherCommand;
use App\Console\Commands\Weather\SyncForecastWeatherByCitiesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule
             ->call(SyncCurrentWeatherCommand::class)
             ->cron('0 5,11,17,23 * * *');

        $schedule
            ->call(SyncForecastWeatherByCitiesCommand::class)
            ->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
