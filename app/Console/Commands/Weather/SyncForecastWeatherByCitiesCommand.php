<?php

declare(strict_types=1);

namespace App\Console\Commands\Weather;

use App\Action\Weather\SyncForecastWeatherByCitiesAction;
use App\Jobs\Weather\SyncForecastWeatherByCitiesJob;
use Illuminate\Console\Command;

class SyncForecastWeatherByCitiesCommand extends Command
{
    protected $signature = 'weather:forecast';

    protected $description = 'Get forecast weather data';

    public function handle(): void
    {
        SyncForecastWeatherByCitiesJob::dispatch();
    }
}
