<?php

declare(strict_types=1);

namespace App\Console\Commands\Weather;

use App\Action\Weather\SyncCurrentWeatherByCitiesAction;
use Illuminate\Console\Command;

class SyncCurrentWeatherCommand extends Command
{
    protected $signature = 'weather:current';

    protected $description = 'Get today\'s weather data';

    public function handle(SyncCurrentWeatherByCitiesAction $action): void
    {
        $action->execute();
    }
}
