<?php

declare(strict_types=1);

namespace App\Console\Commands\City;

use App\Action\City\SyncCityAction;
use Illuminate\Console\Command;

class SyncCitiesCommand extends Command
{
    protected $signature = 'city:sync';

    protected $description = 'Getting coordinates for cities';

    public function handle(SyncCityAction $action): void
    {
        $action->execute();
    }
}
