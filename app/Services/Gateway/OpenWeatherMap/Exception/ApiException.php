<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap\Exception;

use Exception;

class ApiException extends Exception
{
    protected string $responseBody;
    protected array $responseHeaders;

    public function __construct($message = '', $code = 0, $responseHeaders = [], $responseBody = null)
    {
        parent::__construct($message, $code);

        $this->responseHeaders = $responseHeaders;
        $this->responseBody = $responseBody;
    }

    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    public function getResponseBody(): string
    {
        return $this->responseBody;
    }
}
