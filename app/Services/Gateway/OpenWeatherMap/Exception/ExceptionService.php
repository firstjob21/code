<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap\Exception;

use GuzzleHttp\Exception\RequestException;

class ExceptionService
{
    /**
     * @throws ApiException
     */
    public function requestException(RequestException $exception): void
    {
        throw new ApiException(
            "[{$exception->getCode()}] {$exception->getMessage()}",
            $exception->getCode(),
            $exception->getResponse() ? $exception->getResponse()->getHeaders() : null,
            $exception->getResponse() ? $exception->getResponse()->getBody()->getContents() : null
        );
    }

    /**
     * @throws ApiException
     */
    public function handleException(array $params): void
    {
        throw new ApiException(
            sprintf(
                '[%d] Error connecting to the API (%s)',
                $params['code'],
                $params['uri'],
            ),
            $params['code'],
            $params['header'],
            $params['body'],
        );
    }
}
