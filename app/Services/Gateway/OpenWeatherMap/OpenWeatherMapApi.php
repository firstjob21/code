<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap;

use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Services\Gateway\OpenWeatherMap\Models\Model;

class OpenWeatherMapApi implements OpenWeatherMapApiInterface
{
    public function __construct(private OpenWeatherMapClient $openWeatherMapClient)
    {
        //
    }

    public function getWeatherData(Model $model): array
    {
        return $this
            ->openWeatherMapClient
            ->send(
                $this
                    ->openWeatherMapClient
                    ->getRequest(
                        $model->getPath(),
                        $model->getQuery(),
                    )
            );
    }
}
