<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap\Models\Current;

use App\Services\Gateway\OpenWeatherMap\Models\Model;

class Current extends Model
{
    protected const PATH = '/weather';

    protected function rules(): array
    {
        return [
            'q' => 'required|string',
            'mode' => 'nullable|string',
            'units' => 'nullable|string',
            'lang' => 'nullable|string',
        ];
    }
}
