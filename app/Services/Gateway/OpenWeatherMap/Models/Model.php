<?php declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap\Models;

use Illuminate\Support\Facades\Validator;
use Exception;

abstract class Model
{
    protected const PATH = '';
    protected array $query = [];

    abstract protected function rules(): array;

    public function getRules(): array
    {
        return $this->rules();
    }

    public function getPath(): string
    {
        return static::PATH;
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @throws Exception
     */
    public function setQuery(array $data): void
    {
        $this->validate($data, $this->getRules());
        $this->query = $data;
    }

    /**
     * @throws Exception
     */
    public function validate(array $data, array $rules): void
    {
        try {
            Validator
                ::make($data, $rules)
                ->validate();
        } catch (Exception $exception) {
            throw new Exception(json_encode($exception->validator->messages(), JSON_THROW_ON_ERROR));
        }
    }
}
