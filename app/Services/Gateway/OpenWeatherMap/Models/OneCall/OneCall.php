<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap\Models\OneCall;

use App\Services\Gateway\OpenWeatherMap\Models\Model;

class OneCall extends Model
{
    protected const PATH = '/onecall';

    protected function rules(): array
    {
        return [
            'lon' => 'required|numeric',
            'lat' => 'required|numeric',
            'exclude' => 'nullable|numeric',
            'lang' => 'nullable|numeric',
        ];
    }
}
