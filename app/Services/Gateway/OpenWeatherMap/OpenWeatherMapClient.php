<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap;

use App\Services\Gateway\OpenWeatherMap\Exception\ExceptionService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class OpenWeatherMapClient
{
    public function __construct(
        private ClientInterface $client,
        private Configuration $config,
        private ExceptionService $exceptionService,
    ) {
        //
    }

    public function getRequest(string $resourcePath, array $queryParams): Request
    {
        $query = $this->getFullQueryParams($queryParams);

        return new Request(
            'GET',
            $this->config->getUrlApi() . $resourcePath . ($query ? "?{$query}" : ''),
        );
    }

    private function getFullQueryParams(array $queryParams): string
    {
        $queryParams = array_map(
            static fn($value): string => (string) $value,
            array_merge($queryParams, ['appid' => $this->config->getApiKey()])
        );

        return http_build_query($queryParams);
    }

    /**
     * @throws Exception\ApiException
     * @throws GuzzleException
     */
    public function send(Request $request): array
    {
        try {
            $response = $this
                ->client
                ->send($request);
        } catch (RequestException $exception) {
            $this
                ->exceptionService
                ->requestException($exception);
        }

        $this->checkResponse($request, $response);

        return json_decode($response->getBody()->getContents(), true);
    }

    private function checkResponse(Request $request, Response $response): void
    {
        $statusCode = $response->getStatusCode();

        if ($statusCode < 200 || $statusCode > 299) {
            $this
                ->exceptionService
                ->handleException([
                    'code' => $statusCode,
                    'uri' => $request->getUri(),
                    'header' => $response->getHeaders(),
                    'body' => $response->getBody(),
                ]);
        }
    }
}
