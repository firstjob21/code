<?php

declare(strict_types=1);

namespace App\Services\Gateway\OpenWeatherMap;

class Configuration
{
    public function __construct(protected string $apiKey, protected string $urlApi)
    {
        //
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function getUrlApi(): ?string
    {
        return $this->urlApi;
    }
}
