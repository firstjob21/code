<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Exceptions\Weather\WeatherNotFoundException;
use App\Repository\Weather\WeatherRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UpdateWeatherAction
{
    public function __construct(private WeatherRepository $repository) {}

    public function execute(UpdateWeatherDTO $request): UpdateWeatherResponse
    {
        try {
            $weather = $this->repository->getById($request->getId());
        } catch (ModelNotFoundException $exception) {
            throw new WeatherNotFoundException();
        }

        $weather->date = $request->getDate();
        $weather->main_description = $request->getMainDescription();
        $weather->description = $request->getDescription();
        $weather->temp = $request->getTemp();
        $weather->feels_like = $request->getFeelsLike();
        $weather->temp_min = $request->getTempMin();
        $weather->temp_max = $request->getTempMax();
        $weather->pressure = $request->getPressure();
        $weather->humidity = $request->getHumidity();
        $weather->wind_speed = $request->getWindSpeed();
        $weather->wind_deg = $request->getWindDeg();

        $weather = $this->repository->save($weather);

        return new UpdateWeatherResponse($weather);
    }
}
