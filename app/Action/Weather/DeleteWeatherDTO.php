<?php

declare(strict_types=1);

namespace App\Action\Weather;

final class DeleteWeatherDTO
{
    public function __construct(private int $id) {}

    public function getId(): int
    {
        return $this->id;
    }
}
