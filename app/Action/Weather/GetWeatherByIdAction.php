<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Action\GetByIdRequest;
use App\Repository\Weather\WeatherRepository;

class GetWeatherByIdAction
{
    public function __construct(private WeatherRepository $repository) {}

    public function execute(GetByIdRequest $request): GetWeatherByIdResponse
    {
        $weather = $this
            ->repository
            ->getById($request->getId());

        return new GetWeatherByIdResponse($weather);
    }
}
