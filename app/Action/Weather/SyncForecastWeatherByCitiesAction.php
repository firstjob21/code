<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Contracts\City\CityRepositoryInterface;
use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Services\Gateway\OpenWeatherMap\Models\OneCall\OneCall;
use Carbon\Carbon;

class SyncForecastWeatherByCitiesAction
{
    public function __construct(
        private OpenWeatherMapApiInterface $api,
        private CityRepositoryInterface $cityRepository,
        private UpdateOrCreateWeatherAction $updateOrCreateWeatherAction,
    ) {}

    public function execute(): void
    {
        $cities = $this->cityRepository->getAll();

        foreach ($cities as $city) {
            $model = new OneCall();
            $model->setQuery([
                'lon' => $city->getAttribute('longitude'),
                'lat' => $city->getAttribute('latitude'),
            ]);

            $dailyWeather = $this
                ->api
                ->getWeatherData($model)['daily'];

            foreach ($dailyWeather as $dayWeather) {
                $weatherInfo = array_shift($dayWeather['weather']);

                $this
                    ->updateOrCreateWeatherAction
                    ->execute(
                        new UpdateOrCreateWeatherDTO(
                            $weatherInfo['id'],
                            (new Carbon($dayWeather['dt']))->toDateString(),
                            $weatherInfo['main'],
                            $weatherInfo['description'],
                            $dayWeather['temp']['day'],
                            $dayWeather['feels_like']['day'],
                            $dayWeather['temp']['min'] ?? null,
                            $dayWeather['temp']['max'] ?? null,
                            $dayWeather['pressure'],
                            $dayWeather['humidity'],
                            $dayWeather['wind_speed'],
                            $dayWeather['wind_deg'],
                        )
                    );
            }
        }
    }
}
