<?php

declare(strict_types=1);

namespace App\Action\Weather;

final class UpdateWeatherDTO
{
    public function __construct(
        private int $id,
        private string $date,
        private string $mainDescription,
        private string $description,
        private float $temp,
        private float $feelsLike,
        private float $tempMin,
        private float $tempMax,
        private int $pressure,
        private int $humidity,
        private float $windSpeed,
        private int $windDeg
    ) {}

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getMainDescription(): string
    {
        return $this->mainDescription;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getTemp(): float
    {
        return $this->temp;
    }

    /**
     * @return float
     */
    public function getFeelsLike(): float
    {
        return $this->feelsLike;
    }

    /**
     * @return float
     */
    public function getTempMin(): float
    {
        return $this->tempMin;
    }

    /**
     * @return float
     */
    public function getTempMax(): float
    {
        return $this->tempMax;
    }

    /**
     * @return int
     */
    public function getPressure(): int
    {
        return $this->pressure;
    }

    /**
     * @return int
     */
    public function getHumidity(): int
    {
        return $this->humidity;
    }

    /**
     * @return float
     */
    public function getWindSpeed(): float
    {
        return $this->windSpeed;
    }

    /**
     * @return int
     */
    public function getWindDeg(): int
    {
        return $this->windDeg;
    }
}
