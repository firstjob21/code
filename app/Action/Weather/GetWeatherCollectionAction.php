<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Action\PaginatedResponse;
use App\Contracts\Weather\WeatherRepositoryInterface;
use App\Repository\Weather\WeatherRepository;

class GetWeatherCollectionAction
{
    public function __construct(private WeatherRepositoryInterface $repository) {}

    public function execute(GetCollectionDTO $request): PaginatedResponse
    {
        return new PaginatedResponse(
            $this
                ->repository
                ->paginate(
                $request->getPage() ?: WeatherRepository::DEFAULT_PAGE,
                WeatherRepository::DEFAULT_PER_PAGE,
                $request->getSort() ?: WeatherRepository::DEFAULT_SORT,
                $request->getDirection() ?: WeatherRepository::DEFAULT_DIRECTION
            )
        );
    }
}
