<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Models\Weather;
use App\Repository\Weather\WeatherRepository;

final class AddWeatherAction
{
    public function __construct(private WeatherRepository $repository) {}

    public function execute(AddWeatherDTO $request): AddWeatherResponse
    {
        $weather = new Weather([
            'city_id' => $request->getCityId(),
            'date' => $request->getDate(),
            'main_description' => $request->getMainDescription(),
            'description' => $request->getDescription(),
            'temp' => $request->getTemp(),
            'feels_like' => $request->getFeelsLike(),
            'temp_min' => $request->getTempMin(),
            'temp_max' => $request->getTempMax(),
            'pressure' => $request->getPressure(),
            'humidity' => $request->getHumidity(),
            'wind_speed' => $request->getWindSpeed(),
            'wind_deg' => $request->getWindDeg(),
        ]);

        $weather = $this->repository->save($weather);

        return new AddWeatherResponse($weather);
    }
}
