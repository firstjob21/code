<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Models\City;
use App\Services\Gateway\OpenWeatherMap\Models\OneCall\OneCall;
use Carbon\Carbon;

final class SyncCurrentWeatherByCityAction
{
    public function __construct(
        private OpenWeatherMapApiInterface $api,
        private UpdateOrCreateWeatherAction $updateOrCreateWeatherAction,
    ) {}

    public function execute(City $city): void
    {
        $model = new OneCall();
        $model->setQuery([
            'lon' => $city->getAttribute('longitude'),
            'lat' => $city->getAttribute('latitude'),
        ]);

        $currentWeather = $this
            ->api
            ->getWeatherData($model)['current'];

        $weatherInfo = array_shift($currentWeather['weather']);

        $this
            ->updateOrCreateWeatherAction
            ->execute(
                new UpdateOrCreateWeatherDTO(
                    $weatherInfo['id'],
                    (new Carbon($currentWeather['dt']))->toDateString(),
                    $weatherInfo['main'],
                    $weatherInfo['description'],
                    $currentWeather['temp'],
                    $currentWeather['feels_like'],
                    $currentWeather['temp_min'] ?? null,
                    $currentWeather['temp_max'] ?? null,
                    $currentWeather['pressure'],
                    $currentWeather['humidity'],
                    $currentWeather['wind_speed'],
                    $currentWeather['wind_deg'],
                )
            );
    }
}
