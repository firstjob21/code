<?php

declare(strict_types=1);

namespace App\Action\Weather;

use Illuminate\Database\Eloquent\Collection;

class GetWeatherByDateResponse
{
    public function __construct(private Collection $collection) {}

    public function getCollection(): Collection
    {
        return $this->collection;
    }
}
