<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Contracts\City\CityRepositoryInterface;
use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Services\Gateway\OpenWeatherMap\Models\OneCall\OneCall;
use Carbon\Carbon;

final class SyncCurrentWeatherByCitiesAction
{
    public function __construct(
        private OpenWeatherMapApiInterface $api,
        private CityRepositoryInterface $cityRepository,
        private UpdateOrCreateWeatherAction $updateOrCreateWeatherAction,
    ) {}

    public function execute(): void
    {
        $cities = $this->cityRepository->getAll();

        foreach ($cities as $city) {
            $model = new OneCall();
            $model->setQuery([
                'lon' => $city->getAttribute('longitude'),
                'lat' => $city->getAttribute('latitude'),
            ]);

            $currentWeather = $this
                ->api
                ->getWeatherData($model)['current'];

            $weatherInfo = array_shift($currentWeather['weather']);

            $this
                ->updateOrCreateWeatherAction
                ->execute(
                    new UpdateOrCreateWeatherDTO(
                        $weatherInfo['id'],
                        (new Carbon($currentWeather['dt']))->toDateString(),
                        $weatherInfo['main'],
                        $weatherInfo['description'],
                        $currentWeather['temp'],
                        $currentWeather['feels_like'],
                        $currentWeather['temp_min'] ?? null,
                        $currentWeather['temp_max'] ?? null,
                        $currentWeather['pressure'],
                        $currentWeather['humidity'],
                        $currentWeather['wind_speed'],
                        $currentWeather['wind_deg'],
                    )
                );
        }
    }
}
