<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Models\Weather;

class UpdateWeatherResponse
{
    public function __construct(private Weather $weather) {}

    public function getWeather(): Weather
    {
        return $this->weather;
    }
}
