<?php

declare(strict_types=1);

namespace App\Action\Weather;

class GetWeatherByDateDTO
{
    public function __construct(private string $date) {}

    public function getDate(): string
    {
        return $this->date;
    }
}
