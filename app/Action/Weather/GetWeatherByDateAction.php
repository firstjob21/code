<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Exceptions\Weather\WeatherNotFoundException;
use App\Repository\Weather\WeatherRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class GetWeatherByDateAction
{
    public function __construct(
        private WeatherRepository $repository,
        private SyncCurrentWeatherByCitiesAction $syncCurrentWeatherByCitiesAction
    ) {}

    public function execute(GetWeatherByDateDTO $dto): GetWeatherByDateResponse
    {
        $weather = $this->getWeather($dto);

        if ($weather->isEmpty()) {
            $this
                ->syncCurrentWeatherByCitiesAction
                ->execute();
            $weather = $this->getWeather($dto);
        }

        throw_if($weather->isEmpty(), new WeatherNotFoundException());

        return new GetWeatherByDateResponse($weather);
    }

    private function getWeather(GetWeatherByDateDTO $dto): Collection
    {
        return $this
            ->repository
            ->getByDate($dto->getDate());
    }
}
