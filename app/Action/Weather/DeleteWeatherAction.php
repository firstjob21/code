<?php

declare(strict_types=1);

namespace App\Action\Weather;

use App\Exceptions\Weather\WeatherNotFoundException;
use App\Repository\Weather\WeatherRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class DeleteWeatherAction
{
    public function __construct(private WeatherRepository $repository) {}

    public function execute(DeleteWeatherDTO $request): void
    {
        try {
            $weather = $this->repository->getById($request->getId());
        } catch (ModelNotFoundException $exception) {
            throw new WeatherNotFoundException();
        }

        $this->repository->delete($weather);
    }
}
