<?php

declare(strict_types=1);

namespace App\Action\City;

class UpdateOrCreateCityDTO
{
    public function __construct(
        private string $name,
        private float $latitude,
        private float $longitude,
    ) {}

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }
}
