<?php

declare(strict_types=1);

namespace App\Action\City;

use App\Constants\City\CityConstants;
use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Events\CiteUpdateOrCreate;
use App\Services\Gateway\OpenWeatherMap\Models\Current\Current;
use Exception;

final class SyncCityAction
{
    public function __construct(
        private OpenWeatherMapApiInterface $api,
        private UpdateOrCreateCityAction $updateOrCreateCityAction,
    ) {}

    public function execute(): void
    {
        foreach (CityConstants::CITIES as $cityName) {
            $model = new Current();
            $model->setQuery([
                'q' => $cityName,
            ]);

            $weatherData = $this
                ->api
                ->getWeatherData($model);

            throw_if(empty($weatherData), new Exception("$cityName did not found"));

            $cite = $this
                ->updateOrCreateCityAction
                ->execute(
                    new UpdateOrCreateCityDTO(
                        $cityName,
                        $weatherData['coord']['lat'],
                        $weatherData['coord']['lon'],
                    )
                )
                ->getCity();

            CiteUpdateOrCreate::dispatch($cite);
        }
    }
}
