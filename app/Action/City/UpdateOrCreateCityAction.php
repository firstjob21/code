<?php

declare(strict_types=1);

namespace App\Action\City;

use App\Contracts\City\CityRepositoryInterface;

final class UpdateOrCreateCityAction
{
    public function __construct(private CityRepositoryInterface $repository) {}

    public function execute(UpdateOrCreateCityDTO $cityDTO): UpdateOrCreateCityResponse
    {
        $cite = $this
            ->repository
            ->updateOrCreate(
                $cityDTO->getName(),
                [
                    'longitude' => $cityDTO->getLongitude(),
                    'latitude' => $cityDTO->getLatitude(),
                ]
            );

        return new UpdateOrCreateCityResponse($cite);
    }
}
