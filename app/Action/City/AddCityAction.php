<?php

declare(strict_types=1);

namespace App\Action\City;

use App\Contracts\City\CityRepositoryInterface;
use App\Models\City;

final class AddCityAction
{
    public function __construct(private CityRepositoryInterface $repository) {}

    public function execute(AddCityDTO $request): void
    {
        $city = new City([
            'name' => $request->getName(),
            'latitude' => $request->getLatitude(),
            'longitude' => $request->getLongitude(),
        ]);

        $this->repository->save($city);
    }
}
