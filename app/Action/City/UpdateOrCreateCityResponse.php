<?php

declare(strict_types=1);

namespace App\Action\City;

use App\Models\City;

final class UpdateOrCreateCityResponse
{
    public function __construct(private City $city) {}

    public function getCity(): City
    {
        return $this->city;
    }
}
