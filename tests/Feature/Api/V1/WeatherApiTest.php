<?php

declare(strict_types=1);

namespace Tests\Feature\Api\V1;

use App\Models\City;
use App\Models\Weather;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Tests\Feature\Api\ApiTestCase;

final class WeatherApiTest extends ApiTestCase
{
    private const API_URL = 'api/v1/weather';

    private const CONTACT_RESOURCE_STRUCTURE = [
        'city',
        'date',
        'main_description',
        'description',
        'temp',
        'feels_like',
        'temp_min',
        'temp_max',
        'pressure',
        'humidity',
        'wind_speed',
        'wind_deg',
    ];

    public function test_get_weather_collection()
    {
        $this->assertCollectionResponse(self::API_URL, self::CONTACT_RESOURCE_STRUCTURE);
    }

    public function test_get_weather_collection_invalid_query_params()
    {
        $this->assertCollectionErrorResponse(self::API_URL, ['page' => 0]);
    }

    public function test_get_weather_by_id()
    {
        $weather = Weather::first();

        $this->assertItemResponse(
            $this->createResourceItemUri(self::API_URL, $weather->id),
            self::CONTACT_RESOURCE_STRUCTURE
        );
    }

    public function test_get_weather_by_id_not_found()
    {
        $this->assertNotFoundResponse($this->createResourceItemUri(self::API_URL, 999));
    }

    public function test_add_weather()
    {
        $city = City::first();

        $this->assertCreatedResponse(self::API_URL, [
            'city_id' => $city->id,
            'date' => (new Carbon())->toDateString(),
            'main_description' => Str::random(40),
            'description' => Str::random(40),
            'temp' => random_int(0, 500),
            'feels_like' => random_int(-300, 500),
            'temp_min' => random_int(-300, 200),
            'temp_max' => random_int(200, 500),
            'pressure' => random_int(1000, 5000),
            'humidity' => random_int(0, 100),
            'wind_speed' => random_int(0, 10),
            'wind_deg' => random_int(0, 10),
        ]);
    }

    public function test_add_weather_invalid_request_params()
    {
        $this->assertErrorResponse(self::API_URL, []);
        $this->assertErrorResponse(self::API_URL, [
            'city_id' => 999,
        ]);
    }

    public function test_update_weather_by_id()
    {
        $weather = Weather::first();

        $this->assertUpdatedResponse(
            $this->createResourceItemUri(
                self::API_URL,
                $weather->id
            ),
            [
                'date' => (new Carbon())->toDateString(),
                'main_description' => Str::random(40),
                'description' => Str::random(40),
                'temp' => random_int(0, 500),
                'feels_like' => random_int(-300, 500),
                'temp_min' => random_int(-300, 200),
                'temp_max' => random_int(200, 500),
                'pressure' => random_int(1000, 5000),
                'humidity' => random_int(0, 100),
                'wind_speed' => random_int(0, 10),
                'wind_deg' => random_int(0, 10),
            ]
        );
    }

    public function test_delete_weather_by_id()
    {
        $weather = Weather::first();

        $this->assertDeletedResponse(
            $this->createResourceItemUri(self::API_URL, $weather->id)
        );
    }

    public function test_delete_weather_by_id_not_found()
    {
        $this->assertDeleteNotFoundResponse(
            $this->createResourceItemUri(
                self::API_URL,
                999
            )
        );
    }

    public function test_get_weather_by_date()
    {
        $date = (new Carbon())->toDateString();
        $city = City::first();

        Weather::factory()
            ->count(3)
            ->state([
                'date' => $date,
                'city_id' => $city->id,
            ])
            ->create();

        $this->assertCollectionResponse(self::API_URL . "/date/$date", self::CONTACT_RESOURCE_STRUCTURE);
    }
}
