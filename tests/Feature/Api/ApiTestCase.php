<?php

declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Exceptions\ErrorCode;
use App\Models\City;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use InvalidArgumentException;
use Tests\CreatesApplication;

abstract class ApiTestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected const ROOT_RESPONSE_KEY = 'data';

    protected function setUp(): void
    {
        parent::setUp();

        $this->seedFakeData();
    }

    protected function seedFakeData(int $itemsAmount = 5): void
    {
        City::factory()
            ->hasWeather(5)
            ->count($itemsAmount)
            ->create();
    }

    private function assertUriIsValid(string $uri): void
    {
        if (empty($uri)) {
            throw new InvalidArgumentException('Request URI cannot be empty.');
        }
    }

    protected function assertCollectionResponse(string $uri, array $jsonStructure, array $queryParams = []): void
    {
        $this->assertUriIsValid($uri);

        $response = $this->call('GET', $uri, $queryParams)->assertOk();

        $this->assertNotEmpty($response->json('data'));

        $response->assertJsonStructure([
            self::ROOT_RESPONSE_KEY => ['*' => $jsonStructure]
        ]);
    }

    protected function assertCollectionErrorResponse(string $uri, array $queryParams = []): void
    {
        $this->assertUriIsValid($uri);

        $this
            ->call('GET', $uri, $queryParams)
            ->assertJsonStructure(['errors' => []]);
    }

    protected function assertItemResponse(string $uri, array $jsonStructure): void
    {
        $this->assertUriIsValid($uri);

        $this->get($uri)
            ->assertOk()
            ->assertJsonStructure([self::ROOT_RESPONSE_KEY => $jsonStructure]);
    }

    protected function assertNotFoundResponse(string $uri): void
    {
        $this->assertUriIsValid($uri);

        $this->get($uri)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Resource not found.'
                    ]
                ]
            ]);
    }

    protected function assertDeleteNotFoundResponse(string $uri): void
    {
        $this->assertUriIsValid($uri);

        $this->delete($uri)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Resource not found.'
                    ]
                ]
            ]);
    }

    protected function assertErrorResponse(string $uri, array $attributes = [], string $httpMethod = 'POST'): void
    {
        $this->assertUriIsValid($uri);

        $this->json($httpMethod, $uri, $attributes)
            ->assertStatus(400)
            ->assertJsonStructure(['errors' => []]);
    }

    protected function assertCreatedResponse(string $uri, array $attributes): void
    {
        $this->assertUriIsValid($uri);
        $this->assertAttributesIsValid($attributes);

        $this->json('POST', $uri, $attributes)
            ->assertStatus(201)
            ->assertJsonStructure(['data' => ['id']]);
    }

    protected function assertUpdatedResponse(string $uri, array $attributes): void
    {
        $this->assertUriIsValid($uri);
        $this->assertAttributesIsValid($attributes);

        $this->json('PUT', $uri, $attributes)->assertStatus(200);
    }

    protected function assertDeletedResponse(string $uri): void
    {
        $this->assertUriIsValid($uri);

        $this->json('DELETE', $uri)->assertStatus(204);
    }

    protected function createResourceItemUri(string $uri, int $id): string
    {
        $this->assertUriIsValid($uri);

        return $uri . '/' . $id;
    }

    private function assertAttributesIsValid(array $attributes): void
    {
        if (empty($attributes)) {
            throw new InvalidArgumentException('Request attributes are empty.');
        }
    }
}
