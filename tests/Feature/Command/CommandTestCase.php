<?php

declare(strict_types=1);

namespace Tests\Feature\Command;

use App\Models\City;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreatesApplication;

abstract class CommandTestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seedFakeData();
    }

    protected function seedFakeData(int $itemsAmount = 5): void
    {
        City::factory()
            ->hasWeather(5)
            ->count($itemsAmount)
            ->create();
    }
}
