<?php

declare(strict_types=1);

namespace Tests\Feature\Command\Weather;

use App\Jobs\Weather\SyncForecastWeatherByCitiesJob;
use Illuminate\Support\Facades\Queue;
use Tests\Feature\Command\CommandTestCase;

final class WeatherCommandTest extends CommandTestCase
{
    public function test_city_sync_command(): void
    {
        Queue::fake();

        $this->artisan('weather:forecast')->assertSuccessful();

        Queue::assertPushed(SyncForecastWeatherByCitiesJob::class);
    }
}
