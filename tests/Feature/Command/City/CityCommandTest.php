<?php

declare(strict_types=1);

namespace Tests\Feature\Command\City;

use App\Contracts\Gateway\OpenWeatherMapApiInterface;
use App\Events\CiteUpdateOrCreate;
use Illuminate\Support\Facades\Event;
use Mockery\MockInterface;
use Tests\Feature\Command\CommandTestCase;

final class CityCommandTest extends CommandTestCase
{
    public function test_city_sync_command(): void
    {
        Event::fake();

        $this->mock(OpenWeatherMapApiInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('getWeatherData')
                ->andReturn([
                    'coord' => [
                        'lat' => random_int(-200, 500),
                        'lon' => random_int(-200, 500)
                    ]
                ]);
        });

        $this->artisan('city:sync')->assertSuccessful();

        Event::assertDispatched(CiteUpdateOrCreate::class);
    }
}
