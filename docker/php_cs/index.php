<?php

require __DIR__ . '/../../vendor/autoload.php';

$dotenv = \Dotenv\Dotenv::create(__DIR__ . '/../..');

$dotenv->load();

use CodeReview\php_cs\ApiClient;
use CodeReview\php_cs\PhpCsReport;

$phpCsReport = new PhpCsReport(
    new ApiClient
);

$result = $phpCsReport->generate();

if (!$result) {
    echo 'PHP_CodeSniffer validation failed';

    exit(1);
}
