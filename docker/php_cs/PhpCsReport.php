<?php

namespace CodeReview\php_cs;

class PhpCsReport
{
    private $apiClient;
    private $messages = [];

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function generate(): bool
    {
        $this->parseMessages();

        $this->apiClient->deleteExistsReport();

        $this->apiClient->storeReport(
            $this->getReportRequestBody()
        );

        if (count($this->messages) === 0) {
            return true;
        }

        $this->apiClient->storeReportAnnotations(
            $this->getReportAnnotationsRequestBody()
        );

        return false;
    }

    private function parseMessages(): void
    {
        $messagesJson = file_get_contents(__DIR__ . '/output/phpcs.json');

        [, , $messages] = array_values(
            json_decode($messagesJson, true)
        );

        $this->messages = $messages;
    }

    private function getReportRequestBody(): array
    {
        return [
            'title' => 'PHP_CodeSniffer',
            "details" => 'Checking code compliance with PSR-2 standards',
            'reporter' => 'PHP_CodeSniffer',
            'remote_link_enabled' => false,
            'report_type' => 'COVERAGE',
            'result' => count($this->messages) ? 'FAILED' : 'PASSED',
            'data' => [
                [
                    'type' => 'DATE',
                    'title' => 'Created at',
                    'value' => now()->format('Y-m-d\TH:i:s.u\Z'),
                ],
            ],
        ];
    }

    private function getReportAnnotationsRequestBody(): array
    {
        return collect($this->messages)
            ->map(function (array $fileMessages, string $fileName): array {
                return collect($fileMessages)
                    ->map(static function (array $messageItem) use ($fileName): array {
                        return collect($messageItem['message'])
                            ->map(static function (string $message) use ($messageItem, $fileName): array {
                                return [
                                    'title' => $message,
                                    'details' => $message,
                                    'external_id' => md5(str_random()),
                                    'annotation_type' => 'CODE_SMELL',
                                    'result' => 'FAILED',
                                    'summary' => $message,
                                    'severity' => 'HIGH',
                                    'path' => $fileName,
                                    'line' => $messageItem['lineNumber'],
                                ];
                            })
                            ->toArray();
                    })
                    ->flatten(1)
                    ->toArray();
            })
            ->flatten(1)
            ->toArray();
    }
}
