<?php

namespace CodeReview\php_cs;

use GuzzleHttp\Client;

class ApiClient
{
    private const REPORT_NAME = 'php-cs-report';

    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/',
            'headers' => [
                'Authorization' => 'Basic ' . getenv('REVIEW_AUTH_STRING'),
            ],
        ]);
    }

    public function deleteExistsReport(): void
    {
        $this->client->delete(
            $this->getMainUri()
        );
    }

    public function storeReport(array $json): void
    {
        $this->client->put(
            $this->getMainUri(),
            compact('json')
        );
    }

    public function storeReportAnnotations(array $json): void
    {
        $this->client->post(
            "{$this->getMainUri()}/annotations",
            compact('json')
        );
    }

    private function getMainUri(): string
    {
        return collect([
            'repositories',
            getenv('BITBUCKET_WORKSPACE'),
            getenv('BITBUCKET_REPO_SLUG'),
            'commit',
            getenv('BITBUCKET_COMMIT'),
            'reports',
            self::REPORT_NAME,
        ])->join('/');
    }
}
